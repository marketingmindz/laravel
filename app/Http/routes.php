<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/','SlidersController@index');


Route::get('online','OnlineController@index');
Route::get('online/{id}','OnlineController@show');

Route::get('about','AboutController@index');

Route::get('module_register', function () {
   return view('register');
});


Route::get('onsite','OnsiteController@index');
Route::get('onsite/{id}','OnsiteController@show');

Route::get('products','ProductController@index');

Route::get('products/{id}','ProductController@show');

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

//Route for the passwordcontroller (forgot password function)
Route::controllers([
   'password' => 'Auth\PasswordController',
]);

Route::get('contact', 
  ['as' => 'contact', 'uses' => 'ContactController@create']);
Route::post('contact', 
  ['as' => 'contact_store', 'uses' => 'ContactController@store']);

/* ================== Homepage + Admin Routes ================== */

require __DIR__.'/admin_routes.php';

Route::auth();

