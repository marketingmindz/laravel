<?php

namespace App\Http\Controllers;

use DB;

use App\Models\Upload;

use App\trainings;

use Illuminate\Http\Request;

use App\Http\Requests;

class OnlineController extends Controller
{
     public function index()
    {	
        
         $trainings = trainings::all();
    	$onsites = DB::table('training_modules')
         ->select(DB::raw('training_modules.id as tid, certifications.name as cname, training_modules.module_name as tmodule_name, trainings.name as trname , training_modules.date as tdate,training_modules.location as tlocation'))
         ->join('certifications', 'training_modules.certification', '=', 'certifications.id')->join('trainings', 'certifications.training', '=', 'trainings.id')->where('module_name','!=' ,'Onsite')->get();
    	  return view('online.index', compact('onsites','trainings'));
    	  
    }
public function show($id)
    {	 

      $onsites = DB::table('training_modules')
      	 ->select(DB::raw('training_modules.id as tid, certifications.name as cname, training_modules.module_name as tmodule_name, trainings.name as trname, training_modules.demo_video as tdemo_video, training_modules.module_description as tmodule_description, training_modules.date as tdate, training_modules.module_price as tmodule_price ,training_modules.location as tlocation' ))
         ->join('certifications', 'training_modules.certification', '=', 'certifications.id')->where('training_modules.id', $id)->join('trainings', 'certifications.training', '=', 'trainings.id')->where('module_name','!=' , 'Onsite')->get();
    	return view('online.show', compact('onsites'));
    	  
    }
}
