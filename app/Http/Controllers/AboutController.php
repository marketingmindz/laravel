<?php

namespace App\Http\Controllers;


use DB;

use App\Models\Upload;

use Illuminate\Http\Request;

use App\Page;

use App\Http\Requests;

class AboutController extends Controller
{
    public function index()
    {
    	 $pages = DB::table('pages')
         ->join('uploads', 'pages.featured_image', '=', 'uploads.id')->where('pages.id', '=', 1)->get();


        return view('about', compact('pages'));
    	

    }
}
