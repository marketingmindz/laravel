<?php

namespace App\Http\Controllers;

use App\Models\Upload;

use App\training_modules;

use DB;

use Illuminate\Http\Request;

use App\Http\Requests;

class OnsiteController extends Controller
{
    public function index()
    {	   $onsites = DB::table('training_modules')
         ->select(DB::raw('training_modules.id as tid, certifications.name as cname, training_modules.module_name as tmodule_name'))
         ->join('certifications', 'training_modules.certification', '=', 'certifications.id')->where('module_name', 'Onsite')->get();
    	  return view('onsite.index', compact('onsites'));
    	  
    }
public function show($id)
    {	 

      $onsites = DB::table('training_modules')
         ->select(DB::raw('training_modules.id as tid, certifications.name as cname, training_modules.module_name as tmodule_name, trainings.name as trname, training_modules.demo_video as tdemo_video, training_modules.module_description as tmodule_description, training_modules.date as tdate, training_modules.module_price as tmodule_price ,training_modules.location as tlocation' ))
         ->join('certifications', 'training_modules.certification', '=', 'certifications.id')->where('training_modules.id', $id)->join('trainings', 'certifications.training', '=', 'trainings.id')->where('module_name','Onsite')->get();
        return view('online.show', compact('onsites'));
    }

}
