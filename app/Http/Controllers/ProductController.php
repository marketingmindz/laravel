<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;

use App\Http\Requests;

class ProductController extends Controller
{
    public function index()
    {

    	$products = Product::all();
    	return view('products.index', compact('products'));
    }

    public function show($id)
    {

    	$product = Product::findorfail($id);
    	return view('products.show', compact('product'));
    }
}
