<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = [
    'slide_heading',
    'slide_description',
    'slide_image'
    ];
}
