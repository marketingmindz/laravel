<?php

namespace App\Http\Controllers;

use DB;

use App\Models\Upload;

use App\Slider;

use Illuminate\Http\Request;

use App\Http\Requests;

class SlidersController extends Controller
{
    public function index()
    {

    	
    	 $sliders = DB::table('sliders')
         ->join('uploads', 'sliders.slide_image', '=', 'uploads.id')->get();
         return view('welcome', compact('sliders'));
        

    }
}
