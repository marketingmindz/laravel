@extends('app')

@section('content')
 @foreach($pages as $page)
  <!-- Start Inner Banner area -->
            <div class="inner-banner-area">
                <div class="container">
                    <div class="row">
                        <div class="innter-title">
                            <h2>{{ $page->page_title }}</h2>
                        </div>
                        <div class="breadcrum-area">
                            <ul class="breadcrumb">
                                <li><a href="#">Home</a></li>
                                <li class="active">{{ $page->page_title }}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Inner Banner area -->
            <!-- Start About gymedge area -->
            <div class="about-gymedge-area padding-top">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-7 col-sm-7">
                            <div class="about-content-area">
                                <div class="title-section">
                                    <h2>{{ $page->page_title }}</h2>
                                </div>
                                <div class="content-section">
                                    {!!html_entity_decode($page->page_description)!!}
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-5 col-sm-5">
                            <div class="about-img">
                                <img class="img-responsive" src="{{ url('files/'.$page->hash.'/'.$page->name)}}" alt="about-img">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End About gymedge area -->
            
 @endforeach  

@stop