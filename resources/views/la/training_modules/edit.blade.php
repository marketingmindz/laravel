@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/training_modules') }}">Training Module</a> :
@endsection
@section("contentheader_description", $training_module->$view_col)
@section("section", "Training Modules")
@section("section_url", url(config('laraadmin.adminRoute') . '/training_modules'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Training Modules Edit : ".$training_module->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($training_module, ['route' => [config('laraadmin.adminRoute') . '.training_modules.update', $training_module->id ], 'method'=>'PUT', 'id' => 'training_module-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'module_name')
					@la_input($module, 'certification')
					@la_input($module, 'type')
					@la_input($module, 'module_description')
					@la_input($module, 'demo_video')
					@la_input($module, 'date')
					@la_input($module, 'location')
					@la_input($module, 'video1')
					@la_input($module, 'video2')
					@la_input($module, 'video3')
					@la_input($module, 'pdf1')
					@la_input($module, 'pdf2')
					@la_input($module, 'module_price')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/training_modules') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#training_module-edit-form").validate({
		
	});
});
</script>
@endpush
