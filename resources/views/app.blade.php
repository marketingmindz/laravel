<!doctype html>
<html class="no-js" lang="">
    
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Yoga | Home Page</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- favicon
        ============================================ -->        
        
        
        <!-- Bootstrap CSS
        ============================================ -->        
        <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">

        <!-- Font Awesome CSS
        ============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('css/font-awesome.min.css') }}">

        <!-- Owl Caousel CSS 
        ============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('vendor/OwlCarousel/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('vendor/OwlCarousel/owl.theme.default.min.css') }}">

        <!-- meanmenu CSS
        ============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('css/meanmenu.min.css') }}">

        <!-- normalize CSS
        ============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('css/normalize.css') }}">

        <!-- main CSS
        ============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('css/main.css') }}">

        <!-- nivo slider CSS
        ============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('css/custom-slider/css/nivo-slider.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ URL::asset('css/custom-slider/css/preview.css') }}" type="text/css" media="screen" />

        <!-- flaticon CSS
        ============================================ -->
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/flaticon.css') }}"> 

        <!-- Wow CSS
        ============================================ -->
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/animate.css') }}"> 
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/site.css') }}">  

        <!-- Switch Style CSS 
        ============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('css/hover-min.css') }}">

        <!-- Magic popup CSS 
        ============================================-->
        <link rel="stylesheet" href="{{ URL::asset('css/magnific-popup.css') }}">

        <!-- style CSS
        ============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('style.css') }}">

        <!-- responsive CSS
        ============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('css/responsive.css') }}">

        <!-- modernizr JS
        ============================================ -->
        <script src="{{ URL::asset('js/vendor/modernizr-2.8.3.min.js') }}"></script>
    </head>
    <body>
        <!-- Start wrapper -->
        <div class="wrapper">
           <div id="preloader"></div>
        <!-- Preloader End Here -->
        <!-- Start Header area -->
        <header class="main-header header-style3"  id="sticker">
                <div class="header-top-bar" style="margin-bottom: 0px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="top-bar-left">
                                    <ul>
                                        <li><i class="fa fa-phone" aria-hidden="true"></i><a href="Tel:212-302-5651"> 212-302-5651 </a></li>
                                        <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:info@your-movement.com">info@your-movement.com</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="top-bar-right">
                                    <ul>
                                        <li><a href="https://www.facebook.com/YourMovement/?ref=bookmarks"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="https://www.instagram.com/your_movement/?hl=en "><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                        <li><a href="https://plus.google.com/u/0/117609950709259752812"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-top-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="logo-area">
                                    <a href="{{ URL::to('/')}}"><img src="{{ URL::asset('img/logo.png') }}" alt="logo"></a>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-9">
                                <div class="main-menu">
                                    <nav>
                                        <ul>
                                            <li><a href="{{ url ('/')}}">Home</a>
                                            <li><a href="{{ url ('about')}}">About Us</a></li>
                                            <li><a href="{{ url ('online')}}">Online Certification</a></li>
                                            <li><a href="{{ url ('onsite')}}">Onsite Certification</a></li>
                                           
                                            
                                           </li>
                                            @if (Auth::guest())
                                                     <li><a href="{{ url ('register')}}">Login/Register</a>
                                                @else
                                                    <!-- User Account Menu -->
                                                    <li class="dropdown user user-menu">
                                                    <!-- Menu Toggle Button -->
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                        <span class="hidden-xs">Hi {{ Auth::user()->name }}</span>
                                                    </a>
                                                    <ul class="dropdown-menu mm-log">
                                                            <li>
                                                            <div class="pull logout-mm">
                                                                <a href="{{ url('/logout') }}" class="btn btn-default btn-flat">log out</a>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </li>
                                            @endif
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-lg-1 col-md-1 hidden-sm">
                                <div class="header-top-right">
                                    <ul>
                                        <li>
                                            <div class="header-top-search search-box">
                                                <form>
                                                <input class="search-text" type="text"  placeholder="Search Here..." >
                                                <a class="search-button" href="#">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </a>
                                                </form>
                                            </div>
                                        </li>
                                       
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- End header Top Area -->
            <!-- mobile-menu-area start -->
            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul>
                                        <li><a href="#">Home</a></li>
                                        <li><a href="#">About</a></li>
                                        <li><a href="#">Classes</a>
                                            <ul>
                                                <li><a href="#">Single Classes</a></li>
                                            </ul>
                                        </li>
                                        
                                        <li><a href="#">Contact</a></li>                              
                                    </ul>
                                </nav>
                            </div>                  
                        </div>
                    </div>
                </div>
            </div>
            <!-- mobile-menu-area end --> 
        </header>
        <!-- End Header area -->
            <!-- Start slider area  -->
            @yield('content')

            <!-- Start footer Area -->
        <footer>
                <div class="footer-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="about-company">
                                    <h3>STUDENTS STORIES</h3>
                                    <p>Your-Movement believes the best teachers are always learning and continuing to moving forward with their education. With our trainings, and your dedication, we will teach you to be the best teacher you can be.</p>
                                    <div class="social-icons">
                                        <ul class="social-link">
                                            <li class="first">
                                                <a class="facebook" target="_blank" href="https://www.facebook.com/YourMovement/?ref=bookmarks "><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                            </li>
                                            <li class="second">
                                                <a class="twitter" target="_blank" href=" https://www.instagram.com/your_movement/?hl=en"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                            </li>
                                            <li class="third">
                                                <a class="linkedin" href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                            </li>
                                            <li class="fourth">
                                                <a class="pint" target="_blank" href="https://plus.google.com/u/0/117609950709259752812 "><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                            </li>
                                            <li class="five">
                                                <a class="skype" href="#"><i class="fa fa-skype" aria-hidden="true"></i></a>
                                            </li>
                                            <li class="last">
                                                <a class="youtube" href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="twitter-area">
                                    <h3>Twitter Feed</h3>
                                    <div class="twitter-list">
                                        <ul>
                                            <li>
                                            <p><i class="fa fa-twitter" aria-hidden="true"></i>Looking for an awesome CREATIVE WordPress Theme? Esquise  run even better.</p>
                                            <a href="#">http://t.co/0WWEMQEQ48</a>
                                            <p><span>3 Days ago</span></p>
                                            </li>
                                            <li>
                                            <p><i class="fa fa-twitter" aria-hidden="true"></i>Looking for an awesome CREATIVE WordPress Theme? Esquise  run even better.</p>
                                            <a href="#">http://t.co/0WWEMQEQ48</a>
                                            <p><span>3 Days ago</span></p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="flickr-photos">
                                    <h3>Flickr Photos</h3>
                                    <div class="flickr-list">
                                        <ul>
                                            <li><a class="fancybox" href="{{ URL::asset('img/flickr/flickr1.png')}}" data-fancybox-group="flickr" title="flickr1"><img src="{{ URL::asset('img/flickr/flickr1.png')}}" alt="flickr1"></a></li>
                                            <li><a class="fancybox" href="{{ URL::asset('img/flickr/flickr2.png')}}" data-fancybox-group="flickr" title="flickr2"><img src="{{ URL::asset('img/flickr/flickr2.png')}}" alt="flickr2"></a></li>
                                            <li><a class="fancybox" href="{{ URL::asset('img/flickr/flickr3.png')}}" data-fancybox-group="flickr" title="flickr3"><img src="{{ URL::asset('img/flickr/flickr3.png')}}" alt="flickr3"></a></li>
                                            <li><a class="fancybox" href="{{ URL::asset('img/flickr/flickr4.png')}}" data-fancybox-group="flickr" title="flickr4"><img src="{{ URL::asset('img/flickr/flickr4.png')}}" alt="flickr4"></a></li>
                                            <li><a class="fancybox" href="{{ URL::asset('img/flickr/flickr5.png')}}" data-fancybox-group="flickr" title="flickr5"><img src="{{ URL::asset('img/flickr/flickr5.png')}}" alt="flickr5"></a></li>
                                            <li><a class="fancybox" href="{{ URL::asset('img/flickr/flickr6.png')}}" data-fancybox-group="flickr" title="flickr6"><img src="{{ URL::asset('img/flickr/flickr6.png')}}" alt="flickr6"></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="corporate-office">
                                    <h3>Corporate Office</h3>
                                    <div class="corporate-address">
                                        <ul>
                                            <li><i class="fa fa-send" aria-hidden="true"></i>43 W 46th street, 3rd floor, New York, 10036</li>
                                            <li><i class="fa fa-phone" aria-hidden="true"></i>212-302-5651</li>
                                            <li><i class="fa fa-envelope-o" aria-hidden="true"></i>info@your-movement.com</li>
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End footer Area -->
                <!-- Start copyright area -->
                <div class="copy-right-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-9 col-md-9 col-sm-9">
                                <div class="copy-right">
                                    <p>© Copyrights  MM 2016. All rights reserved. Designed by<a href=""> MM</a></p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="visa-card">
                                    <ul>
                                        <li><a href="#"><img src="{{ URL::asset('img/visa-card.png')}}" alt="visa-card"></a></li>
                                        <li><a href="#"><img src="{{ URL::asset('img/descover.png')}}" alt="descover"></a></li>
                                        <li><a href="#"><img src="{{ URL::asset('img/paypal.png')}}" alt="paypal"></a></li>
                                        <li><a href="#"><img src="{{ URL::asset('img/card.png')}}" alt="card"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- End copyright area -->
        </div>
        <!-- End wrapper -->
        <a href="#" class="scrollToTop"></a>


        <!-- jquery
        ============================================ -->        
        <script src="{{ URL::asset('js/vendor/jquery-1.11.3.min.js') }}"></script>

        <!-- bootstrap JS
        ============================================ -->        
        <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('js/bootstrap-tabcollapse.js') }}"></script> 

        <!-- meanmenu JS
        ============================================ -->        
        <script src="{{ URL::asset('js/jquery.meanmenu.min.js') }}"></script>

        <!-- Owl Cauosel JS 
        ============================================ --> 
        <script src="{{ URL::asset('vendor/OwlCarousel/owl.carousel.min.js') }}" type="text/javascript"></script>

        <!-- Nivo slider js
        ============================================ -->        
        <script src="{{ URL::asset('css/custom-slider/js/jquery.nivo.slider.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('css/custom-slider/home.js') }}" type="text/javascript"></script>

        <!-- Zoom JS
        ============================================ -->
        <script src="{{ URL::asset('js/jquery.zoom.js') }}"></script>

        <!-- Isotope JS
        ============================================ -->
        <script src="{{ URL::asset('js/isotope.pkgd.js') }}"></script>

        <!-- Counter Up JS
        ============================================ -->
        <script src="{{ URL::asset('js/waypoints.min.js') }}"></script>
        <script src="{{ URL::asset('js/jquery.counterup.min.js') }}"></script>

        <!-- Magic Popup js 
        ============================================-->
        <script src="{{ URL::asset('js/jquery.magnific-popup.min.js') }}" type="text/javascript"></script>

        <!-- Wow JS
        ============================================ -->
        <script src="{{ URL::asset('js/wow.min.js') }}"></script>

        <!-- plugins JS
        ============================================ -->
        <script src="{{ URL::asset('js/plugins.js') }}"></script>

        <!-- main JS
        ============================================ -->
        <script src="{{ URL::asset('js/main.js') }}"></script>

    </body>


</html>
