@extends('app')

@section('content')

<div class="inner-banner-area">
    <div class="container">
      <div class="row">
        <div class="innter-title">
          <h2>Login/Register</h2>
        </div>
        <div class="breadcrum-area">
          <ul class="breadcrumb">
            <li><a href="{{ URL::to('/')}}">Home</a></li>
            <li class="active">Login/Register</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- End Inner Banner area --> 
  <!-- Start details classes area -->
  <div class="classes-detail-area padding-top">
    <div class="container">
      <div class="single-class-detail">
        <div class="class-content">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="log-in-full-part">
                <div class="log-in-right">
                  <h1>Log In</h1>
                 
                  <form role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}" >
                                  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" id="exampleInputEmail1" placeholder="Email">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                           

                            
                                <input id="password" type="password" class="form-control" name="password" id="exampleInputEmail1" placeholder="Password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            
                        </div>

                        <div class="form-group">
                           
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            
                        </div>

                        <div class="form-group">
                            
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-btn fa-sign-in"></i> Login
                                </button>

                                <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                            
                        </div>
                    </form>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="log-in-full-part">
                <div class="log-in-right">
                  <h1> Registration</h1>
                   <form role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                           

                                <input id="name" type="text" class="form-control" name="name" placeholder="Name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                    </div>
                    <div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}">
                           

                                <input id="user_name" type="text" class="form-control" name="user_name" placeholder="User Name" value="{{ old('user_name') }}">

                                @if ($errors->has('user_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user_name') }}</strong>
                                    </span>
                                @endif
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                           

                                <input id="email" type="text" class="form-control" name="email" placeholder="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                    </div>
                     <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                           

                                <input id="password" type="text" class="form-control" name="password" placeholder="password" value="{{ old('password') }}">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                    </div>
                    
                     <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                           

                                 <input type="password" class="form-control" placeholder="Retype password" name="password_confirmation"/>
                               
                    </div>
                    <div class="form-group{{ $errors->has('mail_1') ? ' has-error' : '' }}">
                           

                                <input id="mail_1" type="text" class="form-control" name="mail_1" placeholder="Mailing address" value="{{ old('mail_1') }}">

                                @if ($errors->has('mail_1'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mail_1') }}</strong>
                                    </span>
                                @endif
                    </div>
                    <div class="form-group{{ $errors->has('zip_code') ? ' has-error' : '' }}">
                           

                                <input id="zip_code" type="text" class="form-control" name="zip_code" placeholder="Zip/Postal COde" value="{{ old('zip_code') }}">

                                @if ($errors->has('zip_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('zip_code') }}</strong>
                                    </span>
                                @endif
                    </div>
                    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                           

                                <input id="city" type="text" class="form-control" name="city" placeholder="City" value="{{ old('city') }}">

                                @if ($errors->has('city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
                    </div>
                    <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                           

                                <input id="state" type="text" class="form-control" name="state" placeholder="State" value="{{ old('state') }}">

                                @if ($errors->has('state'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('state') }}</strong>
                                    </span>
                                @endif
                    </div>
                    <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                           

                                <input id="country" type="text" class="form-control" name="country" placeholder="Country" value="{{ old('country') }}">

                                @if ($errors->has('country'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                @endif
                    </div>
                   
                     

                  
                  
                    <div class="form-group">
                            
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-btn fa-user"></i> Register
                                </button>
                            
                        </div>
                    
                    
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Start Related classes --> 
    
    <!-- End Related classes --> 
  </div>
</div>
</div>
</div>
</div>

@endsection
