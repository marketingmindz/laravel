\@extends('app')

@section('content')
            <div class="inner-banner-area">
                <div class="container">
                    <div class="row">
                        <div class="innter-title">
                            <h2>Training Register</h2>
                        </div>
                        <div class="breadcrum-area">
                            <ul class="breadcrumb">
                                <li><a href="#">Home</a></li>
                                <li class="active">Register</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Inner Banner area -->
 <div class="classes-detail-area padding-top">
    <div class="container">
      <div class="single-class-detail">
        <div class="class-content">
          <div class="class-heading">
            <h3>COURSE FILTER:</h3>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="left-part">
                <form>
                  <div class="checkbox">
                   
                      Mat Pilates Training  <br>

    Basic Pilates Teacher Certification ONLINE   
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 1  </label>
                  </div>
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 2  </label>
                  </div>
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 3  </label>
                  </div>
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 4  </label>
                  </div>
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 5  </label>
                  </div>
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">    
                      Module 6 </label>
                  </div>

    Intermediate Pilates Teacher Certification ONLINE  
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 1 </label>
                  </div>  
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 2  </label>
                  </div>
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 3  </label>
                  </div>
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 4  </label>
                  </div>
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 5 </label>
                  </div> 
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 6 </label>
                  </div> 
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 7  </label>
                  </div>
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Basic/Intermediate Pilates Teacher Training ONSITE 
</label>
                  </div>
    Props Pilates  <br>

    Props Pilates Teacher Certification ONLINE 

   <div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 1 </label>
                  </div>  
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 2  </label>
                  </div>
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 3  </label>
                  </div>
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 4  </label>
                  </div>
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 5 </label>
                  </div> 
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 6 </label>
                  </div> 
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 7  </label>
                  </div>
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 8  </label>
                  </div>
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 9  </label>
                  </div>
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 10  </label>
                  </div>

<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Props Pilates Teacher Trainings ONSITE  
</label>
                  </div>
    Postnatal Trainings  
    <br>
    Postnatal Teacher Certification ONLINE  

    <div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 1 </label>
                  </div>  
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 2  </label>
                  </div>
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 3  </label>
                  </div>
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 4  </label>
                  </div>
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 5 </label>
                  </div> 
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 6 </label>
                  </div> 
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 7  </label>
                  </div>
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 8  </label>
                  </div>
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Module 9  </label>
                  </div> 
<div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
    Postnatal Teacher Training ONSITE 
</label>
                  </div>

                  <br>
                  <select class="form-control issue">
                    <option>-- Any State/Region --</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                  <br>
                  <select class="form-control issue">
                    <option>-- Any Date -- </option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                  <br>
                  <button type="button" class="btn btn-success">Success</button>
                </form>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
             
            </div>
          </div>
        </div>
        <!-- Start Related classes --> 
        
        <!-- End Related classes --> 
      </div>
    </div>
  </div>

@stop


