@extends('app')

@section('content')
            <div class="inner-banner-area">
                <div class="container">
                    <div class="row">
                        <div class="innter-title">
                            <h2>Onsite Certifications</h2>
                        </div>
                        <div class="breadcrum-area">
                            <ul class="breadcrumb">
                                <li><a href="{{ URL::to('/')}}">Home</a></li>
                                <li class="active">Onsite Certifications</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Inner Banner area -->


    <div class="our-classes-area padding-top">
                 <div class="container">
                    <div class="row">
                        <div class="col-lg-9 col-md-9">
                            
                        </div>
                         <div class="col-lg-3 col-md-3">
                         <form method="GET">
                         
                            <input type="text" class="keyword-mm" name="keyword" placeholder="Enter Location/Date">
                            <i class="fa fa-search" aria-hidden="true"></i>
                            
                        </form>
                         </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <!-- Gallery Section Area Start Here -->
                            <div class="gallery-area">
                               
                                <div class="portfolioContainer zoom-gallery"> 

                                @foreach($onsites as $onsite)              
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <div class="single-classes-area">
                                            <div class="classes-img">
                                                <a href="{{ url ('onsite' , $onsite->tid)}}">
                                                    <img src="img/classes/yoga.jpg" alt="yoga">
                                                </a>
                                                <div class="classes-overlay">
                                                    <a  href="{{ url ('onsite' , $onsite->tid)}}" title="Classic Yoga"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                            <div class="classes-title">
                                                <h3><a href="{{ url ('onsite' , $onsite->tid)}}">{{ $onsite->cname }}</a></h3>
                                                <p class="date">{{ $onsite->tmodule_name }}</p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach    
                                                         
                                </div>
                            </div>
                        <!-- Gallery Section Area End Here -->
                    </div>
                </div>
            </div>
@stop


