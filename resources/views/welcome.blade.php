@extends('app')

@section('content')

  <div class="slider-area bg-slider nav-slider slider3-caption slider-top-space-header3">
                <div class="bend niceties preview-2">
                    <div id="ensign-nivoslider-2" class="slides"> 
                    {{--*/ $i = 1 /*--}}
                     @foreach($sliders as $slider)

                      <img src="{{ url('files/'.$slider->hash.'/'.$slider->name)}}" alt="image{{ $i }}" title="#slider-direction-{{ $i }}"  />

                      {{--*/ $i++ /*--}}
                        @endforeach  
     
                    </div>
                       {{--*/ $var = 1 /*--}}

                        @foreach($sliders as $slider)

                       <!-- direction 1 -->
                    <div id="slider-direction-{{ $var }}" class="t-cn slider-direction">
                        <!-- <div class="slider-progress"></div> -->
                        <div class="slider-content t-cn s-tb slider-1">
                            <div class="title-container s-tb-c title-compress">
                                <div data-wow-delay="0.1s" data-wow-duration="1s" class="tp-caption big-title rs-title customin customout bg-sld-cp-primary ">{{ $slider->slide_heading }}
                                </div>
                            <div data-wow-delay="0.2s" data-wow-duration="2s" class="tp-caption small-content customin customout rs-title-small bg-sld-cp-primary tp-resizeme rs-parallaxlevel-0 ">
                           {{ $slider->slide_description }}
                            </div>
                            </div>
                            <div class="button"><a href="#" class="btn custom-button" data-title="Join With Us">Join With Us</a></div>
                        </div>  
                    </div>
                        {{--*/ $var++ /*--}}
                        @endforeach                    
                     
                </div>
            </div>
            <!-- Start First area -->
           <!-- Start First area -->
            <div class="about-fitness-area">
                <div class="container-fluid">
                    <div class="about-fitness-left">
                         <img src="img/about-fitness-img-01.png" alt="about-fitness-img">
                    </div>
                    <div class="about-fitness-right padding-space">
                        <div class="about-single-service">
                            <div class="media service-item">
                                
                                <div class="media-body service-content">
                                
                                    <h2>MAT PILATES TRAINING </h2>
                                    <p>Our Mat training consists of two parts <b>basic</b>, which will teach you all beginner Pilates poses and tips on encouraging students to gain ab strength, and <b>intermediate</b>, which is a leveling up in skill and difficulty. After completing the Pilate’s Mat Training the range of clients we can work with is expanded profoundly, as well at the over-all teaching ability and bodily strength.</p>
                                    
                                    <h2 class="learn">LEARN MORE</h2>
                                    
                                    <h2><a href="{{ url ('module_register')}}" class="register">REGISTER ></a></h2>
                                    
                                    
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
            </div>
            <!-- End First area -->   
           



<div class="about-fitness-area">
                <div class="container-fluid">
                    
                   
                   
                    <div class="about-fitness-right padding-space">
                        <div class="about-single-service">
                            <div class="media service-item">
                                
                                <div class="media-body service-content">
                                
                                <h2>PROPS PILATES TRAINING</h2>
                                    
                                    <p>Props Training is our most extensive training. The program covers beginners, intermediate, and advanced level exercises and expands upon their success and difficulty with the incorporation of props. Endeavoring to make even the most classical exercises contemporary and fun.</p>
                                    <h2 class="learn">LEARN MORE</h2>
                                    <h2><a href="{{ url ('module_register')}}" class="register">REGISTER ></a></h2>
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                    
                    
                    <div class="about-fitness-left">
                    
                     <img src="img/about-fitness-img.png" alt="about-fitness-img">
                        
                    </div>
                    
               </div>
            </div>
            
            
            <div class="about-fitness-area">
                <div class="container-fluid">
                    
                   <div class="about-fitness-left">
                       <img src="img/about-fitness-img-02.png" alt="about-fitness-img">
                    </div>
                   
                    <div class="about-fitness-right padding-space">
                        <div class="about-single-service">
                            <div class="media service-item">
                                
                                <div class="media-body service-content">
                                
                                    <h2>POSTNATAL TRAININGS</h2>
                                    
                                    <p>Originally designed as a way to help moms get their toned tummy back, this course has grown in understanding and capacity. This course focuses on Special Case Clients (i.e. repairing diastis recti, regain abdominal strength, recovering after surgery or illness, pre- and post-natal women, etc) and shows teachers how to react affectively to help meet the needs of all students. </p>
                                    <h2 class="learn">LEARN MORE</h2>
                                    <h2><a href="{{ url ('module_register')}}" class="register">REGISTER ></a></h2>
                                    
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                    
                    
                    
                </div>
            </div>
            
            
            
            <!-- End First area -->   
           
 <!-- Manish Create section -->
            
            <section class="upcoming">
                <div class="container">
                    <div class="row">
                        <div class="program">
                            <h3>UPCOMING TRAININGS</h3>
                            <h4>REGISTER FOR A TRAINING PROGRAM OR CONFERENCE </h4>
                        </div>
                    <div class="col-md-4">
                        <div class="training">
                            <h5>Basic and Intermediate Pilates Training</h5>
                            <p>April 15-16 and April 22-23  10am-5pm</p>
                            <a href="{{ url ('onsite')}}" class="read"> Read More >> </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="training">
                            <h5>Props Pilates Training</h5>
                            <p>May 13-14 and May 20-21 10-5pm</p>
                            <a href="{{ url ('onsite')}}" class="read"> Read More >> </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="training">
                            <h5>Postnatal Training </h5>
                            <p>June 12-16 10am-5pm</p>
                            <a href="{{ url ('onsite')}}" class="read"> Read More >> </a>
                        </div>
                    </div>
                    
                </div>
            </div>
            </section>
            <!-- End Manish Section -->
                 
        
           
         
         
           
            <!-- Counter Area Start Here -->
            <div class="counter-area" style="background-image: url('img/banner/counter-back.jpg');">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 counter1-box wow fadeInDown" data-wow-duration=".5s" data-wow-delay=".20s">
                            <h2 class="counter">1520</h2>
                            <p>Satisfied Customers</p>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 counter1-box wow fadeInDown" data-wow-duration=".5s" data-wow-delay=".40s">
                            <h2 class="counter">23</h2>
                            <p>Trainers</p>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 counter1-box wow fadeInDown" data-wow-duration=".5s" data-wow-delay=".60s">
                            <h2 class="counter">199</h2>
                            <p>Received Awards</p>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 counter1-box wow fadeInDown" data-wow-duration=".5s" data-wow-delay=".80s">
                            <h2 class="counter">130</h2>
                            <p>Equipments</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Counter Area End Here -->                       
            <!-- Start latest news area -->
            <div class="latest-news-area nav-on-hover">
                <div class="container">
                    <div class="section-title">
                        <h2>Latest News</h2>
                    </div>
                </div>
                <div class="container">
                    <div class="gym-carousel nav-control-top"
                        data-loop="true"
                        data-items="3"
                        data-margin="30"
                        data-autoplay="false"
                        data-autoplay-timeout="10000"
                        data-smart-speed="2000"
                        data-dots="false"
                        data-nav="true"
                        data-nav-speed="false"
                        data-r-x-small="1"
                        data-r-x-small-nav="true"
                        data-r-x-small-dots="false"
                        data-r-x-medium="1"
                        data-r-x-medium-nav="true"
                        data-r-x-medium-dots="false"
                        data-r-small="2"
                        data-r-small-nav="true"
                        data-r-small-dots="false"
                        data-r-medium="3"
                        data-r-medium-nav="true"
                        data-r-medium-dots="false"
                        data-r-large="3"
                        data-r-large-nav="true"
                        data-r-large-dots="false">
                         {{--*/ $i = 1 /*--}}
                         @foreach($blogs as $blog)
                         @php 
                          $date = date('d M Y', strtotime($blog->date)); 
                          $date = explode(' ' , $date);
                         @endphp 
                    
                        <div class="single-latest-news">
                            <div class="single-news">
                                <div class="single-image"><img src="{{ url('files/'.$blog->hash.'/'.$blog->name)}}" alt="news{{$i}}"></div>
                                <div class="date">{{$date[0]}}<br>{{$date[1]}}<br>{{$date[2]}}</div>
                            </div>
                            <div class="news-content">
                                <h3><a href="single-news.html"> {{$blog->title}}</a></h3>
                                <p>{{$blog->discription}}</p>
                                 
                            </div>
                        </div>
                         {{--*/ $i++ /*--}}
                          @endforeach


                        <!-- <div class="single-latest-news">
                            <div class="single-news">
                                <div class="single-image"><img src="img/news2.jpg" alt="news2"></div>
                                <div class="date">20<br>Jan<br>2016</div>
                            </div>
                            <div class="news-content">
                                <h3><a href="single-news.html">Zumba Fitness</a></h3>
                                <p>BodyCombat is the empowering cardio workout are where you are exercitation ullamco totally unleashed. This fiercely ...</p>
                            </div>
                        </div>
                        <div class="single-latest-news">
                            <div class="single-news">
                                <div class="single-image"><img src="img/news3.jpg" alt="news3"></div>
                                <div class="date">20<br>Jan<br>2016</div>
                            </div>
                            <div class="news-content">
                                <h3><a href="single-news.html">Running Practice</a></h3>
                                <p>BodyCombat is the empowering cardio workout are where you are exercitation ullamco totally unleashed. This fiercely ...</p>
                            </div>
                        </div>
                        <div class="single-latest-news">
                            <div class="single-news">
                                <div class="single-image"><img src="img/news1.jpg" alt="news1"></div>
                                <div class="date">20<br>Jan<br>2016</div>
                            </div>
                            <div class="news-content">
                                <h3><a href="single-news.html">Body Combat</a></h3>
                                <p>BodyCombat is the empowering cardio workout are where you are exercitation ullamco totally unleashed. This fiercely ...</p>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <!-- End latest news area -->

 
@stop