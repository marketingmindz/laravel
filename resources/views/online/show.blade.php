@extends('app')

@section('content')

            <div class="inner-banner-area">
                <div class="container">
                @foreach($onsites as $onsite)
                    <div class="row">
                        <div class="innter-title">
                            <h2>{{ $onsite->tmodule_name }} Training</h2>
                        </div>
                        <div class="breadcrum-area">
                            <ul class="breadcrumb">
                                <li><a href="#">Home</a></li>
                                <li class="active">{{ $onsite->tmodule_name }}</li>
                            </ul>
                        </div>
                    </div>
                    @endforeach 
                </div>
            </div>
            <!-- End Inner Banner area -->



     <!-- Start details classes area -->
            <div class="classes-detail-area padding-top">
                <div class="container">
                <div class="class-heading-top">
                  <h3>{{ $onsite->cname }}</h3>
                 </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-8">
                            <div class="single-class-detail">
                                <div class="class-content">

                                @php
                                $data = $onsite->tdemo_video;    
                                $videoid = substr($data, strpos($data, "=") + 1);    
                                @endphp


                               
                                    <div class="detail-img">
                                        <div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://www.youtube.com/embed/{{ $videoid }}?ecver=2" width="867" height="424" frameborder="0" style="position:absolute;width:100%;height:100%;left:0" allowfullscreen></iframe></div>
                                    </div>
                                    <div class="class-heading">
                                        <h3>Description</h3>
                                        
                                    </div>
                                    <div class="content">
                                        <p>{{ $onsite->tmodule_description }}</p>
                                    </div>
                                    
                                </div>
                            </div>
                            <!-- Start Related classes -->
                            
                            <!-- End Related classes -->
                        </div>
                        
                        
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="whole-part">
                                <div class="row">
                       
                                     <div class="col-md-6">
                                        <div class="course">
                                            <strong> Course Number </strong>
                                        </div>
                                    </div>
                        
                                    <div class="col-md-6">
                                        <div class="course">
                                            <span>YM {{ $onsite->tid }}</span>
                                        </div>

                                     </div>
                        
                                 </div>
                        
                                <br>

                        
                        <div class="row">
                        <div class="col-md-6">
                        <div class="course">
                        <strong> Release Date </strong>
                        </div>
                        </div>
                        
                        <div class="col-md-6">
                        <div class="course">
                        <span>{{ date('d M Y', strtotime($onsite->tdate)) }}</span>
                        </div>

                        
                        
                        
                        
                        </div>
                        
                        
                        
                        
                        </div>
                        
<br>
                        <div class="row">
                        <div class="col-md-6">
                        <div class="course">
                        <strong> Price </strong>
                        </div>
                        </div>
                        
                        <div class="col-md-6">
                        <div class="course">
                        <span> ${{ $onsite->tmodule_price }} </span>
                        </div>

                        
                        
                        
                        
                        </div>
                        
                        
                        
                        
                        </div>
    
    
    <br>
                        <div class="row">
                        <div class="col-md-6">
                        <div class="course">
                        <strong> Credit </strong>
                        </div>
                        </div>
                        
                        <div class="col-md-6">
                        <div class="course">
                        <span> View </span>
                        </div>

                        
                        
                        
                        
                        </div>
                        
                        
                        
                        
                        </div>
                      
                      <br>
                    <div class="row">
                       
                                     <div class="col-md-6">
                                        <div class="course">
                                            <strong> Location </strong>
                                        </div>
                                    </div>
                        
                                    <div class="col-md-6">
                                        <div class="course">
                                            <span>{{ $onsite->tlocation }}</span>
                                        </div>

                                     </div>
                        
                                 </div>
                        
                                <br>
                        

                        <input type="button" value="@if (Auth::guest()) submit @else buy @endif" class="submit"> </button>   
                        
                        
                        </div>
                        </div>
                        </div>
                        
                        
                        
                        
                    </div>
                </div>
            </div>
            
            
           

            
            
            
            <!-- End details classes area -->
@stop


