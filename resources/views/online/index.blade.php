@extends('app')

@section('content')
            <div class="inner-banner-area">
                <div class="container">
                    <div class="row">
                        <div class="innter-title">
                            <h2>Online Certifications</h2>
                        </div>
                        <div class="breadcrum-area">
                            <ul class="breadcrumb">
                                <li><a href="{{ URL::to('/')}}">Home</a></li>
                                <li class="active">Online Certifications</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Inner Banner area -->
 <!-- Start Classes page area -->
            <div class="our-classes-area padding-top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-9 col-md-9">
                            <div class="isotop-classes-tab">
                             <a href="#" data-filter="*" class="current">All</a>
                             @foreach($trainings as $training)

                                @php
                                $data = $training->name;    
                                $dataid = substr($data, 0, strpos($data, " "));    
                                @endphp                             
                                <a href="#" data-filter=".{{ $dataid }}">{{ $training->name }}</a>
                               @endforeach  
                            </div>
                        </div>
                         <div class="col-lg-3 col-md-3">
                         <form method="GET">
                         
                            <input type="text" class="keyword-mm" name="keyword" placeholder="Enter Location/Date" value="{{ app('request')->input('keyword') }}">
                            <i class="fa fa-search key-search" aria-hidden="true"></i>
                            
                        </form>
                         </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                     <div class="class-heading-top">
                        <h3>To get certified you need to complete all the modules in each training</h3>
                    </div></div></div>
                <div class="container">
                    <div class="row">
                        <!-- Gallery Section Area Start Here -->
                            <div class="gallery-area">
                               
                                <div class="portfolioContainer zoom-gallery">  
                                    @php
                                    $location = app('request')->input('keyword')  
                                    @endphp  
                                    @if($location!='')  
                                      {{--*/ $i = 1 /*--}}
                                        @foreach($onsites as $onsite)
                                             @php 

                                              $date = date('d M Y', strtotime($onsite->tdate)); 
                                             @endphp 
                                            @if(strtolower($onsite->tlocation) == strtolower($location) || strtolower($date )== strtolower($location))
                                                @php
                                                $data = $onsite->trname;    
                                                $dataid1 = substr($data, 0, strpos($data, " "));    
                                                @endphp                
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 {{ $dataid1 }}">
                                                    <div class="single-classes-area">
                                                        <div class="classes-img">
                                                            <a href="{{ url ('online' , $onsite->tid)}}">
                                                                <img src="img/classes/yoga.jpg" alt="yoga">
                                                            </a>
                                                            <div class="classes-overlay">
                                                                <a  href="{{ url ('online' , $onsite->tid)}}" title="Classic Yoga"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                            </div>
                                                        </div>
                                                        <div class="classes-title">
                                                            <h3><a href="{{ url ('online' , $onsite->tid)}}">{{ $onsite->cname }}</a></h3>
                                                            <p class="date">{{ $onsite->tmodule_name }}</p>
                                                           
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                {{--*/ $i++ /*--}}
                                            @endif
                                            
                                         @endforeach 
                                         @if($i == 1) 
                                         <div class="no-module"> 
                                            <span>No module found<span>
                                        </div>
                                         @endif
                                         
                                    @else
                                        @foreach($onsites as $onsite)
                                            @php
                                            $data = $onsite->trname;    
                                            $dataid1 = substr($data, 0, strpos($data, " "));    
                                            @endphp                
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 {{ $dataid1 }}">
                                                <div class="single-classes-area">
                                                    <div class="classes-img">
                                                        <a href="{{ url ('online' , $onsite->tid)}}">
                                                            <img src="img/classes/yoga.jpg" alt="yoga">
                                                        </a>
                                                        <div class="classes-overlay">
                                                            <a  href="{{ url ('online' , $onsite->tid)}}" title="Classic Yoga"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                    <div class="classes-title">
                                                        <h3><a href="{{ url ('online' , $onsite->tid)}}">{{ $onsite->cname }}</a></h3>
                                                        <p class="date">{{ $onsite->tmodule_name }}</p>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach    
                                    @endif                          
                                </div>
                            </div>
                        <!-- Gallery Section Area End Here -->
                    </div>
                </div>
            </div>
            <!-- End Classes page area -->
@stop


