@extends('app')

@section('content')
<h1>Products</h1>
<hr>
@foreach($products as $product)
<article>
	<h2><a href="{{ url ('products' , $product->id)}}" >{{ $product->product_name }}</a></h2>

</article>

@endforeach

@stop


@section('footer')
<p>Footer Section<p/>

@stop