@extends('app')

@section('content')
<h1>{{ $product->product_name }}</h1>
<hr>
<article>
		<p>{{ $product->product_description }}</p>
		<p>{{ $product->price }}</p>
		<p>{{ $product->product_image }}</p>
</article>

@stop


@section('footer')
<p>Footer Section<p/>

@stop